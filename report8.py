import matplotlib.pyplot as plt
import numpy as np

def sub_matrix(new, old1, old2):
	for n in range(len(old1)):
		new.append(old1[n]-old2[n])
	return new

def concentration(array):
	
	hold_array = []
	
	for n in range(len(array)):
		hold_array.append(array[n]*100)

	return hold_array


def equi_const(conc_prod, conc_react1, conc_react2):
	return (conc_prod)/(conc_react1*conc_react2)



abs_data = [0.005, 0.030, 0.073, 0.075, 0.189]
y_data = []

for n in abs_data:
	x = (n-0.0154)/0.0278
	y_data.append(x*10**(-5))


m_FeSCN = 113.9274 #g/mol
m_FeNO33 = 241.86 #g/mol
m_KSCN = 97.181 #g/mol

mols_used = [0, 5.25e-8, 2.07e-7, 2.14e-7, 6.24e-7]
mols_Fe_start = [1.0e-5]*5
mols_SCN_start = [2.0e-6, 4.0e-6, 6.0e-6, 8.0e-6, 1.0e-5]

place_hold1 = []
place_hold2 = []

mols_at_end_Fe  = sub_matrix(place_hold1, mols_Fe_start, mols_used)
mols_at_end_SCN = sub_matrix(place_hold2, mols_SCN_start, mols_used)


final_conc_Fe = concentration(mols_at_end_Fe)
final_conc_SCN = concentration(mols_at_end_SCN)

K_c = []

for n in range(len(final_conc_Fe)):
	K_c.append(equi_const(y_data[n], final_conc_Fe[n], final_conc_SCN[n]))


k_c = []   #to check the difference when the bad data was not included

for n in range(len(K_c)):
	if n > 0:
		k_c.append(K_c[n])

print np.std(K_c)
print np.mean(K_c)
print np.std(k_c)
print np.mean(k_c)

	

